//
//  HostingController.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 10/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<SessionView> {
    override var body: SessionView {
        return SessionView()
    }
}
