//
//  MeasureUnit.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 12/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

var hour:  [String] {
    let hourInt = Array(0...23)
    let hourString = hourInt.map { String(format:"%02d", $0) }
    return hourString
}

var minsec:  [String] {
    let minsecInt = Array(0...59)
    let minsecString = minsecInt.map { String(format:"%02d", $0) }
    return minsecString
}

enum MeasureUnitType: String {
    case hours   = "MINUTES"
    case minutes = "SECONDS"
    case seconds = "CYCLES"
    
    var indices: Range<Int> {
        switch self {
        case .hours:
            return hour.indices
        case .minutes:
            return minsec.indices
        case .seconds:
            return minsec.indices
        }
    }
    
    var values: [String] {
        switch self {
        case .hours:
            return hour
        case .minutes:
            return minsec
        case .seconds:
            return minsec
        }
    }
}
