//
//  RingViewController.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 10/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI
import UIKit


class RingViewController: WKHostingController<AnyView> {
    override var body: AnyView {
        return AnyView(PercentageRing(ringWidth: 10, percent: 20, backgroundColor: .blue, foregroundColors: [.white, .blue]))
    }
}
