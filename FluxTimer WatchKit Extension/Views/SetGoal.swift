//
//  ContentView.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 10/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//



import SwiftUI

//var lightBlue: Color = Color.init(red: 0.0, green: 245.0, blue: 234.0)

struct SetGoal: View {
    
    @State var goalTime = UserDefaults.standard.double(forKey: "Goal")
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

     
     var body: some View {
          VStack(spacing:0) {
               
               Text("DAILY FOCUS GOAL")
                    .font(.system(size: 16, design: .rounded))
                    .fontWeight(.light)
                    .foregroundColor(lightBlue)
                    .padding()
               Spacer(minLength: 10)
               
               HStack(spacing:0) {
                    Button(action: {
                        if self.goalTime > 0 {
                            self.goalTime -= 5
                         }
                    }) {
                         Image(systemName: "minus")
                              .foregroundColor(.black)
                              .font(Font.body.weight(.heavy))
                         
                    }
                    .frame(width: 35, height: 35, alignment: .center)
                    .background(lightBlue)
                    .cornerRadius(.infinity)
                    
                    Text("\(Int(goalTime))")
                         .font(.system(size: 45, design: .rounded))
                         .fontWeight(.bold)
                         .foregroundColor(Color.white)
                         .multilineTextAlignment(.center)
                         .lineLimit(1)
                         .frame(width: 85, height: 30, alignment: .center) // to review
                         .layoutPriority(1) //to review
                         .focusable(true)
                         .digitalCrownRotation($goalTime, from: 0, through: 990, by: 5, sensitivity: .medium, isContinuous: true, isHapticFeedbackEnabled: true)
                    
                    Button(action: {
                         if self.goalTime < 990 {
                            self.goalTime += 5
                         }
                    }) {
                         Image(systemName: "plus")
                              .foregroundColor(.black)
                            .padding()
                            .font(Font.body.weight(.heavy))
                    }
                    .padding()
                    .frame(width: 35, height: 35, alignment: .center)
                    .background(lightBlue)
                    .cornerRadius(.infinity)
               }
               .padding(2.0)
               .frame(width: 150.0)
               
               Text("MINUTES")
                    .font(.system(.body, design: .rounded))
                    .fontWeight(.regular)
                    .foregroundColor(lightBlue)
                    .padding(.top, -5.0)
               
               Spacer(minLength: 30)
                
            Button(action: {
                print(self.goalTime)
                UserDefaults.standard.set(self.goalTime, forKey: "Goal")
                self.mode.wrappedValue.dismiss()
            }) {
                Text("Save")
                    .fontWeight(.medium)
                    .foregroundColor(.black)
            }
            .background(lightBlue)
            .cornerRadius(.infinity)
          }
          .padding()
     }
}

struct SetGoal_Previews: PreviewProvider {
     static var previews: some View {
        SetGoal()
    }
}
