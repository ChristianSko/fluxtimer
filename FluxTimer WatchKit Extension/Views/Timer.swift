//
//  Timer.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 11/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import SwiftUI
import UserNotifications

struct TimerView: View {

    @State var totalTimerCycles = 1
    @State var currentTimerCycles = 1
    @State var timeRemaining: Int = 10
    @State private var showingActionSheet = false
    @State var goBackSession = false
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State private var focusedTime = UserDefaults.standard.double(forKey: "Focused")
    
    @State var time = Timer.publish(every: 1, on: .main
           , in: .common).autoconnect()
    
    
    var body: some View {
        VStack {
            VStack{
                Text("\(timeToString(from: TimeInterval(timeRemaining)))")
                    .font(Font.monospacedDigit(.system(size: 50))())
                    .fontWeight(.heavy)
                    .foregroundColor(Color.white)
                    .padding(.top, 70.0)
                
                
                Text("\(currentTimerCycles)/\(totalTimerCycles)")
            }
            
            HStack{
                VStack {
                    Button(action: {
    
                        self.mode.wrappedValue.dismiss()
                        print("\(self.timeRemaining)")
                    }) {
                        Text("Stop")
                    }
                    .foregroundColor(.black)
                    .background(lightBlue)
                    .cornerRadius(.infinity)
                    .padding(.vertical, 20.0)
                }
            }
        }
//        .onReceive(self.time) { (_) in
//                if self.timeRemaining > 0 {
//                    self.timeRemaining -= 1
//                    print("hello")
//                    if self.timeRemaining == 0 {
//                        self.focusedTime += 25
//                        UserDefaults.standard.set(self.focusedTime, forKey: "Focused")
//                        print(self.focusedTime)
//                        self.mode.wrappedValue.dismiss()
//                    }
//                }
//            }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("Timer")
        .onAppear(){
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.sound,.alert]) { (_, _) in
            }
            
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
                if self.timeRemaining > 0 {
                    self.timeRemaining -= 1
                    print("hello")
                    if self.timeRemaining == 0 {
                        self.Notify()
//                        self.focusedTime += 25
                        UserDefaults.standard.set(self.focusedTime, forKey: "Focused")
                        print(self.focusedTime)
                        self.mode.wrappedValue.dismiss()
                    }
                }
            }
        }
    }
    
    func Notify(){
             
             let content = UNMutableNotificationContent()
             content.sound = UNNotificationSound.default
             content.title = "Focus Session Completed"
             content.body = "Small break?"
            
             
             let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
             
             let req = UNNotificationRequest(identifier: "MSG", content: content, trigger: trigger)
             
             UNUserNotificationCenter.current().add(req, withCompletionHandler: nil)
         }
}

struct ClockView_Previews: PreviewProvider {
    static var previews: some View {
            TimerView()
                .previewDevice("Apple Watch Series 4 - 44mm")
    }
}


func timeToString(from timeInterval: TimeInterval) -> String {
     
     let seconds = Int(timeInterval.truncatingRemainder(dividingBy: 60))
     let minutes = Int(timeInterval.truncatingRemainder(dividingBy: 60 * 60) / 60)
     
     return String(format: "%.2d:%.2d", minutes, seconds)
     
}
