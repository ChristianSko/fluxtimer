//
//  Personalize.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 11/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import SwiftUI
import UIKit

/*
 Here is embedded the "TimePicker", a customized picker (see the file "TimePicker.swift")
 Here are declared 3 TimePickers: one for hours, one for minutes and one for seconds.
 The labels 🏷 and values 🔢 of the picker are managed by the TimePicker.
 Just need to declare if it works for hours, minutes or seconds.
 
 ⚠️ ATTENTION: What's the "Spacer()" you'll see below?
 Try to comment it and see the preview
 */

struct PickerView: View {
    
    var body: some View {
        VStack {
            HStack {
                TimePicker(measureUnit: .hours)
                Text(":").padding(.horizontal, -4)
                TimePicker(measureUnit: .minutes)
                Text(":").padding(.horizontal, -4)
                TimePicker(measureUnit: .seconds)
            }.edgesIgnoringSafeArea(.horizontal)
            Spacer()
            VStack {
                Button(action: {
                    // The action 🤖 to perform when the button is tapped.
                }) {
                    Text("Start")
                        .colorInvert()
                }
                .background(lightBlue)
                .cornerRadius(20)
                Button(action: {
                    // The action 👾 to perform when the button is tapped.
                }) {
                    Text("Cancel")
                }
                .background(Color(UIColor.darkGray))
                .cornerRadius(20)
            }
            .padding(.horizontal, 4)
            .padding(.bottom, 4)
            // Have you ever heard about this padding?
            // It is just about adding empty space 🛰 in the direction you choose
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}


struct PickerView_Previews: PreviewProvider {
    static var previews: some View {
            PickerView()
                .previewDevice("Apple Watch Series 4 - 44mm")
        }
    }

