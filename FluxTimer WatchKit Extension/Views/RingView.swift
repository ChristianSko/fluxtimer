//
//  SessionView.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 10/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import SwiftUI


let lightBlue: Color = .blue


struct TimeFocusRing: View {
    
    @State var timeFocus = false
    @State private var circleProgress: CGFloat = -0.5
    @State private var goalTime = UserDefaults.standard.double(forKey: "Goal")
    
    var body: some View {
        ScrollView {
            VStack{
                ZStack {
                    Circle()
                        .stroke(lightBlue, lineWidth: 12)
                        .frame(width: 160, height: 160, alignment: .center )
                        .opacity(0.1)
                    Circle()
                        .trim(from: 0, to: circleProgress)
                        .stroke(lightBlue, style: StrokeStyle(lineWidth: 12, lineCap: .round, lineJoin: .round))
                        .frame(width: 160, height: 160, alignment: .center)
                        .rotationEffect(.degrees(270))
                        .animation(Animation.easeIn(duration: 2.5))
                        .onAppear() {
                            self.timeFocus.toggle()
                            self.circleProgress = self.timeFocus ? 0.5 : 1
                    }
                    .overlay(
                        VStack{
                            HStack {
                                Image(systemName: "hourglass")
                                    .padding(.bottom, -9)
                                    .padding([.bottom, .trailing], 2.0)
                                    .foregroundColor(lightBlue)
                                
                                
                                Text("Focus")
                                    .font(.system(.body,design: .rounded))
                                    .fontWeight(.bold)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading, .bottom], -9.0)
                            }
                            
                            HStack {
                                Image(systemName: "")
                                Text("\(Int(0))/\(Int(goalTime))M")
                                    .font(.system(.body,design: .rounded))
                                    .fontWeight(.light)
                                    .foregroundColor(lightBlue)
                                    .multilineTextAlignment(.center)
                                
                                HStack{
                                    Text("\(Int(0))%")
                                        .font(.body)
                                        .fontWeight(.bold)
                                        .multilineTextAlignment(.leading)
                                        .padding(.leading, -9.0)
                                }
                            }
                            .padding(.top, -10)
                        }
                    )
                }
                .padding()
            }
            .padding()
        }
        .navigationBarTitle("Daily")
    }
}

struct TimeFocusRing_Previews: PreviewProvider {
    static var previews: some View {
        TimeFocusRing()
        }
}

