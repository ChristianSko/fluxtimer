//
//  Test.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 11/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import SwiftUI
import Combine


struct SessionView: View {
     
    @State var showSetGoal = false
    @State var showPersonalize = false
    @State var showWeeklySummary = false
    @State var showTimerView = false
    @State var timerTime = 1500
    

    var body: some View {
        ScrollView {
            HStack{
                NavigationLink(destination: TimerView(timeRemaining: 20)) {
                    VStack {
                      Group{
                        Text("")
                        .fontWeight(.light)
                        .font(.footnote)
                        .padding(.bottom, -7.0)

                        Text("25")
                        .font(.system(size: 30, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(lightBlue)
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(width: 85, height: 30, alignment: .center)
                        
                        Text("MIN")
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .padding(.top, -8.0)

                     }
                    }
                }
                .frame(width: 68, height: 68, alignment: .center)
                .cornerRadius(.infinity)
                .overlay(Circle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4,dash: [1.2])))
                .padding()
                
                
                NavigationLink(destination: TimerView(totalTimerCycles: 1, currentTimerCycles: 1,timeRemaining: 2700)) {
                    VStack {
                        Text("")
                        .fontWeight(.light)
                        .font(.footnote)
                        .padding(.bottom, -7.0)

                        Text("45")
                        .font(.system(size: 30, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(lightBlue)
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(width: 85, height: 30, alignment: .center)
                        
                        Text("MIN")
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .padding(.top, -8.0)
                    }
                }
                .frame(width: 68, height: 68, alignment: .center)
                .cornerRadius(.infinity)
                .overlay(Circle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4,dash: [1.2])))
                .padding()
            }
            
            HStack{
                NavigationLink(destination: TimerView(totalTimerCycles: 2, currentTimerCycles: 1,timeRemaining: 1500)) {
                    VStack {
                        Text("2x")
                        .fontWeight(.light)
                        .font(.footnote)
                        .padding(.bottom, -7.0)

                        Text("25")
                        .font(.system(size: 30, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(lightBlue)
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(width: 85, height: 30, alignment: .center)
                        
                        Text("MIN")
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .padding(.top, -8.0)
                    }
                }
                .frame(width: 68, height: 68, alignment: .center)
                .cornerRadius(.infinity)
                .overlay(Circle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4,dash: [1.2])))
                .padding()
                
                
                NavigationLink(destination: TimerView(totalTimerCycles: 2, currentTimerCycles: 1,timeRemaining: 2700)) {
                    VStack {
                        Text("2x")
                        .fontWeight(.light)
                        .font(.footnote)
                        .padding(.bottom, -7.0)

                        Text("45")
                        .font(.system(size: 30, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(lightBlue)
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(width: 85, height: 30, alignment: .center)
                        
                        Text("MIN")
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .padding(.top, -8.0)
                    }
                }
                .frame(width: 68, height: 68, alignment: .center)
                .cornerRadius(.infinity)
                .overlay(Circle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4,dash: [1.2])))
                .padding()
            }
            
            HStack{
                NavigationLink(destination: TimerView(totalTimerCycles: 4, currentTimerCycles: 1, timeRemaining: 1500)) {
                    VStack {
                        Text("4x")
                        .fontWeight(.light)
                        .font(.footnote)
                        .padding(.bottom, -7.0)

                        Text("25")
                        .font(.system(size: 30, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(lightBlue)
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(width: 85, height: 30, alignment: .center)
                        
                        Text("MIN")
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .padding(.top, -8.0)
                    }
                }
                .frame(width: 68, height: 68, alignment: .center)
                .cornerRadius(.infinity)
                .overlay(Circle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4,dash: [1.2])))
                .padding()
                
                
                NavigationLink(destination: TimerView(totalTimerCycles: 4, currentTimerCycles: 1, timeRemaining: 2700)) {
                    VStack {
                        Text("4x")
                        .fontWeight(.light)
                        .font(.footnote)
                        .padding(.bottom, -7.0)

                        Text("45")
                        .font(.system(size: 30, design: .rounded))
                        .fontWeight(.bold)
                        .foregroundColor(lightBlue)
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(width: 85, height: 30, alignment: .center)
                        
                        Text("MIN")
                        .fontWeight(.light)
                        .foregroundColor(Color.white)
                        .padding(.top, -8.0)
                    }
                }
                .frame(width: 68, height: 68, alignment: .center)
                .cornerRadius(.infinity)
                .overlay(Circle()
                .strokeBorder(style: StrokeStyle(lineWidth: 4,dash: [1.2])))
                .padding()
                
            }
            
            NavigationLink(destination: PickerView()) {
                HStack {
                    Image(systemName: "hourglass")
                    Text("Personalize")
                }
            }
            .foregroundColor(.black)
            .background(lightBlue)
            .cornerRadius(.infinity)
        }
//        .listStyle(CarouselListStyle())
        .navigationBarTitle("Session")
        .navigationBarBackButtonHidden(true)
        .contextMenu(menuItems: {
                Button(action: {
                    self.showSetGoal.toggle()
                }, label: {
                    VStack{
                        Image(systemName: "arrow.up.arrow.down.circle")
                            .font(.title)
                        Text("Change daiy focus goal")
                    }
                })
                .sheet(isPresented: $showSetGoal, content: {SetGoal()})
                Button(action: {
                    self.showWeeklySummary.toggle()
                }, label: {
                    VStack{
                        Image(systemName: "calendar.circle")
                            .font(.title)
                        Text("Weekly Summary")
                    }
                })
                .sheet(isPresented: $showWeeklySummary, content: {WeeklySummary()})
            })
        }
    }

struct SetSession_Previews: PreviewProvider {
    static var previews: some View {
        SessionView()
    }
}
