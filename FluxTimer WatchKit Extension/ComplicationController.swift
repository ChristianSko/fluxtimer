//
//  ComplicationController.swift
//  FluxTimer WatchKit Extension
//
//  Created by Christian Skorobogatow on 10/02/2020.
//  Copyright © 2020 Christian Skorobogatow. All rights reserved.
//

import ClockKit
import Foundation
import SwiftUI
import WatchKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration`
    
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.forward, .backward])
    }
    
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        
        // Determine the complication's family.
        switch(complication.family) {
            
        // Handle the modular small family.
        case .modularSmall:

            // Construct a template that displays an image and a short line of text.
//            let template = CLKComplicationTemplateModularSmallRingText()

            // Set the data providers for Ring Style.
//            template.ringStyle = CLKComplicationRingStyle.open
//            template.textProvider = CLKSimpleTextProvider(text: "FLX")
//            template.fillFraction = 0.2
//
            let template = CLKComplicationTemplateModularSmallStackImage()
            
            // Set the data providers.
            template.line1ImageProvider = CLKImageProvider(onePieceImage: UIImage(named: "Complication/Modular")! )
            template.line2TextProvider = CLKSimpleTextProvider(text: "Flux" ,
                                                               shortText: "Flux")
            
            // Create the timeline entry.
            let entry = CLKComplicationTimelineEntry(date: Date(),
                                                     complicationTemplate: template)
            
            // Pass the timeline entry to the handler.
            handler(entry)
            
            
        // Handle other supported families here.

        case .modularLarge:
            let template = CLKComplicationTemplateModularLargeTallBody()
            
            // Set the data providers.
            template.headerTextProvider  = CLKSimpleTextProvider(text: "Flux Timer")
            template.bodyTextProvider = CLKSimpleTextProvider(text: "00:23:11")

            // Create the timeline entry.
            let entry = CLKComplicationTimelineEntry(date: Date(),
                                                     complicationTemplate: template)
            // Pass the timeline entry to the handler.
            handler(entry)
            
            
        // Handle any non-supported families.
        default:
            handler(nil)
        }
    }
    
    
    
//    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
//        switch complication.family {
//        case .modularSmall:
//            let template = CLKComplicationTemplateModularSmallSimpleImage()
//            template.imageProvider = CLKImageProvider (onePieceImage: UIImage(named: "Modular")!)
//            template.tintColor = UIColor.cyan
//
//            let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
//            handler(entry)
//
//
//        case .modularLarge:
//            let template = CLKComplicationTemplateModularLargeTallBody()
//            template.headerTextProvider = CLKSimpleTextProvider(text: "Focus - F1")
//            template.body1TextProvider = CLKSimpleTextProvider(text: "Focus - F1")
//            template.body2TextProvider = CLKSimpleTextProvider(text: "Break - F2")
//
//            let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
//            handler(entry)
//
//        default:
//            return
//        }
//    }
    
    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Placeholder Templates
    
    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        // This method will be called once per supported complication, and the results will be cached
        CLKSimpleTextProvider.localizableTextProvider(withStringsFileTextKey: "AKey")
        handler(nil)
    }
    
    func getPlaceholderTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        var template: CLKComplicationTemplate?

        switch complication.family {
            case .modularSmall:
            template = CLKComplicationTemplateModularSmallRingImage()
            (template as! CLKComplicationTemplateModularSmallRingImage).imageProvider =
                CLKImageProvider(onePieceImage: UIImage(named: "Modular")!)

        case .modularLarge:
            template = CLKComplicationTemplateModularLargeTallBody()
            (template as! CLKComplicationTemplateModularLargeTallBody).headerTextProvider = CLKSimpleTextProvider(text: "F")

        default:
            template = nil
        }
        handler(template)
    }
    
}

//Initial Code That appears

//class ComplicationController: NSObject, CLKComplicationDataSource {
//
//    // MARK: - Timeline Configuration
//
//    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
//        handler([.forward, .backward])
//    }
//
//    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
//        handler(nil)
//    }
//
//    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
//        handler(nil)
//    }
//
//    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
//        handler(.showOnLockScreen)
//    }
//
//    // MARK: - Timeline Population
//
//    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
//        // Call the handler with the current timeline entry
//        handler(nil)
//    }
//
//    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
//        // Call the handler with the timeline entries prior to the given date
//        handler(nil)
//    }
//
//    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
//        // Call the handler with the timeline entries after to the given date
//        handler(nil)
//    }
//
//    // MARK: - Placeholder Templates
//
//    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
//        // This method will be called once per supported complication, and the results will be cached
//        handler(nil)
//    }
//
//}
